
import apiRequest from "../components/apiRequest";

// Constants
// ----------------------------------------------

export const SEARCH_PHOTOS_START = 'SEARCH_PHOTOS_START';
export const SEARCH_PHOTOS_SUCCESS = 'SEARCH_PHOTOS_SUCCESS';
export const SEARCH_PHOTOS_FAILURE = 'SEARCH_PHOTOS_FAILURE';


// Actions
// ----------------------------------------------

/**
 * This function calls sends request to API to get the photos.
 **/
export function searchPhotos(query, page = 1) {
    return (dispatch) => {
        dispatch(startSearchPhotos());
        const url = `https://api.unsplash.com/search/photos?query=${query}&page=${page}`;

        try {
            apiRequest('GET', url).then(
                response => {
                    dispatch(successSearchPhotos(response.data));
                }
            );
        } catch (err) {
            return dispatch(failureSearchPhotos(err));
        }
    }
};

export const startSearchPhotos = () => ({
    type: SEARCH_PHOTOS_START,
    data: [],
});

export const successSearchPhotos = data => ({
    type: SEARCH_PHOTOS_SUCCESS,
    data,
});

export const failureSearchPhotos = error => ({
    type: SEARCH_PHOTOS_FAILURE,
    error,
});

