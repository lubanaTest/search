import React, { Component } from 'react';
import PropTypes from 'prop-types';

/**
 * This component renders an error page with a message
 */
class Error extends Component {

    /**
     * render
     * @returns {*}
     */
    render() {
        const { message } = this.props;

        return (
            <div className="error">
                {message}
            </div>
        );
    }
}

Error.propTypes = {
    message: PropTypes.string
};

Error.defaultProps = {
    message: 'Sorry something went wrong ...'
};

export default Error;