import React, { Component } from 'react';
import ReactLoading from 'react-loading';

/**
 * This component renders a loading page with a spinner when the data is still loading
 */
class Loading extends Component {
    /**
     * render
     * @returns {*}
     */
    render() {
        return (
            <div className="loading">
                <ReactLoading type="spinningBubbles" color="#61AEE1" />
            </div>
        );
    }
}

export default Loading;