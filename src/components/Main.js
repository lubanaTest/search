import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Loading from './Loading';
import Error from './Error';
import { searchPhotos } from "../actions/photos.js";

// Images Library
import Lightbox from 'react-lightbox-component';
import "react-lightbox-component/build/css/index.css";

/**
 * This component renders the main page
 */
class Main extends Component {

  /**
   * Constructor
   * @param props
   */
  constructor(props) {
    super(props);
    this.state = {
      searchTag: '',
      page: 1
    }
  };

  /**
   * onChange
   * This function is called when the user changes the text in the input
   * @param e
   */
  onChange = (e) => {
    this.setState({ searchTag: e.target.value });
  }

  /**
   * search
   * This function calls the search photoa API 
   */
  search = () => {
    const { searchTag, page } = this.state;
    this.props.searchPhotos(searchTag, page);
  }

  /**
   * prev
   * This function is called when the user clicks on prev button 
   * to get photos in the previous page
   */
  prev = () => {
    this.setState({ page: this.state.page > 1 ? this.state.page - 1 : this.props.photos.total_pages }, () => this.search())
  }

  /**
   * prev
   * This function is called when the user clicks on prev button 
   * to get photos in the next page
   */
  next = () => {
    const total = this.props.photos ? this.props.photos.total_pages : 0;
    this.setState({ page: this.state.page === total ? 1 : this.state.page + 1 }, () => this.search())
  }

  /**
   * render
   */
  render() {

    const { searchTag } = this.state;
    const { photos, loading, error } = this.props;

    let loadingMessage = '';
    if (loading) {
      loadingMessage = <Loading />;
    }

    let errorMessage = '';
    if (error) {
      errorMessage = <Error />;
    }

    if (photos && photos.results) {
      photos.results.forEach(photo => {
        photo.src = photo.urls.small;
      });
    }

    return (
      <div className="App">

        <div className="header">
          <h1>Search Photos</h1>
        </div>

        <div className="content">
          <div className="search">
            <span className="text">Please enter your query to start search:</span>
            <input className="form-control form-control-sm" type="text" placeholder="Search" value={searchTag} onChange={(e) => this.onChange(e)} />
            <button className="btn btn-primary go-button" onClick={() => this.search()} >GO</button>
          </div>

          <div>
            <div className="center-text">
              {photos && !photos.results && !loading && <span className="text">Enter your query to start search</span>}
              {photos && photos.results && !loading && photos.total === 0 && <span className="text">No Matching photos</span>}
            </div>
            
            {loadingMessage}
            {errorMessage}
            {photos && photos.results && photos.results.length > 0 &&

              <div className="results">
                <span className="text">Results <b>({photos.total})</b> photos:</span>
                <hr />
                <Lightbox images={photos.results} />
                <hr />

                <button className="btn btn-default" onClick={() => this.prev()} >PREV</button>
              &nbsp;
              <button className="btn btn-default" onClick={() => this.next()} >NEXT</button>
              </div>
            }
          </div>
        </div>
      </div>)
  };
}

const mapStateToProps = state => {
  return {
    photos: state.photos.photos,
    loading: state.photos.loading,
    error: state.photos.error
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    searchPhotos: (query, page) => dispatch(searchPhotos(query, page)),
  }
};

Main.propTypes = {
  photos: PropTypes.object,
  loading: PropTypes.bool,
  error: PropTypes.string
};

Main.defaultProps = {
  photos: {},
  loading: true,
  error: ''
};

export default connect(mapStateToProps, mapDispatchToProps)(Main);
