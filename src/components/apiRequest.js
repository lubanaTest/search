/**
 * This component calls the API endpoint using axios package
 */
import axios from 'axios';

const apiRequest = (method, url, requestBody = null) => {

    const headers = {
        'Accept-Version': 'v1',
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Client-ID 3RNB9SMigUl_44ZqGbceJBTd_geI6sOFriKnHelNT9M'
    };

    return axios({
        method,
        url,
        data: requestBody,
        headers,
    }).then(res => {
        return res;
    }).catch((error) => {
        console.log("The API Request returned an error");
        return [];
    });
};

export default apiRequest;
