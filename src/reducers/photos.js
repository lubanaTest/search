
import {
    SEARCH_PHOTOS_START,
    SEARCH_PHOTOS_SUCCESS,
    SEARCH_PHOTOS_FAILURE
} from './../actions/photos.js'

const initialState = {
    loading: false,
    error: null,
    photos: {},
};

const photos = (state = initialState, action) => {
    switch (action.type) {
      
        case SEARCH_PHOTOS_START:
            return {
                ...state,
                photos: {},
                loading: true,
                error: null,
            };
        case SEARCH_PHOTOS_SUCCESS:
            return {
                ...state,
                photos: action.data,
                loading: false,
                error: null,
            };
        case SEARCH_PHOTOS_FAILURE:
            return { ...state, loading: false, error: 'error' };

        default:
            return state;
    }
};

export default photos;
